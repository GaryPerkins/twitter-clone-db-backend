/* User Gary follows Laura */

START TRANSACTION;
INSERT INTO followers VALUES (1, 2);
UPDATE users 
SET num_followed = num_followed + 1
WHERE users.id = 1;
UPDATE users
SET num_followers = num_followers + 1
WHERE users.id = 2;
COMMIT;