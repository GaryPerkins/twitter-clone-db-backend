/* Get Gary's timeline */

(SELECT username, content, time_tweeted AS Time, "No" AS Retweeted, NULL AS original_tweeter FROM tweets
JOIN users ON tweets.user_who_tweeted = users.id
JOIN followers ON tweets.user_who_tweeted = followers.id_followed
WHERE followers.id_followed IN (SELECT id_followed FROM followers WHERE id_follower = 1))
UNION
(SELECT u1.username, content, time_retweeted AS Time, "Yes" AS Retweeted, u2.username AS original_tweeter FROM retweets
JOIN tweets ON retweets.tweet = tweets.id
JOIN users AS u1 ON retweets.user_who_retweeted = u1.id
JOIN users AS u2 ON tweets.user_who_tweeted = u2.id
JOIN followers ON retweets.user_who_retweeted = followers.id_followed
WHERE followers.id_followed IN (SELECT id_followed FROM followers WHERE id_follower = 1))
ORDER BY Time DESC, username;