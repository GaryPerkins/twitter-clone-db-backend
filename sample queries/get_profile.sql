/** Return Gary's profile */

(SELECT username, content, time_tweeted AS Time, "No" AS Retweeted, NULL AS original_tweeter FROM tweets
JOIN users ON users.id = tweets.user_who_tweeted
WHERE tweets.id = 1)
UNION
(SELECT u2.username AS username, content, time_retweeted AS Time, "Yes" AS Retweeted, u1.username AS original_tweeter FROM tweets
JOIN retweets ON tweets.id = retweets.tweet
JOIN users AS u1 ON u1.id = tweets.user_who_tweeted
JOIN users AS u2 ON u2.id = retweets.user_who_retweeted
WHERE user_who_retweeted = 1)
ORDER BY Time DESC;