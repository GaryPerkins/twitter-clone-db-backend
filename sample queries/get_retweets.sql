/* Get Gary's retweets */

SELECT u1.username AS retweeter, u2.username AS original_tweeter, content, time_retweeted AS Time FROM retweets
JOIN tweets ON retweets.tweet = tweets.id
JOIN users AS u1 ON user_who_retweeted = u1.id
JOIN users AS u2 ON tweets.user_who_tweeted = u2.id
WHERE retweets.user_who_retweeted = 1 
ORDER BY Time DESC, original_tweeter;