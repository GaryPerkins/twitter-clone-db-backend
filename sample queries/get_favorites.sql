/* Get Gary's favorites */

SELECT u2.username AS "User Who Favorited", u1.username AS "User Who Tweeted", content, time_tweeted AS Time FROM tweets
JOIN favorites ON favorites.tweet = tweets.id
JOIN users AS u1 ON tweets.user_who_tweeted = u1.id
JOIN users AS u2 ON favorites.user_who_favorited = u2.id
WHERE favorites.user_who_favorited = 1
ORDER BY Time DESC, u1.username;