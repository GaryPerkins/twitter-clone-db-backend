/* Get Gary's tweets */

SELECT username, content, time_tweeted FROM tweets
JOIN users ON user_who_tweeted = users.id
WHERE user_who_tweeted = 1
ORDER BY time_tweeted DESC;