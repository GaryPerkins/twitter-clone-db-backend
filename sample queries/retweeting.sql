/* Gary retweets Remy's tweet */

START TRANSACTION;
INSERT INTO retweets VALUES (1, DEFAULT, 6);
UPDATE users SET num_retweets = num_retweets + 1 WHERE users.id = 1;
UPDATE tweets SET num_retweets = num_retweets + 1 WHERE tweets.id = 6;
COMMIT;