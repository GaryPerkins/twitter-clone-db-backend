/* Gary makes a tweet */

START TRANSACTION;
INSERT INTO tweets VALUES (DEFAULT, 1, "Hello world!", DEFAULT, DEFAULT, DEFAULT);
UPDATE users SET num_tweets = num_tweets + 1 WHERE users.id = 1;
COMMIT;