/* Gary favorites a tweet by Remy */

START TRANSACTION;
INSERT INTO favorites VALUES (1, 6);
UPDATE users SET num_favorites = num_favorites + 1 WHERE users.id = 1;
UPDATE tweets SET num_favorites = num_favorites + 1 WHERE tweets.id = 6;
COMMIT;