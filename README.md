# Twitter Clone Database Backend #

Backend for a hobby Twitter clone I never ended up implementing with UX. Database design principles of normalization are demonstrated. Knowledge of SQL queries are aptly demonstrated, especially in more complex queries such as returning a user's timeline.

## Instructions ##
* Execute create_db.sql to create the database to be worked on
* Test out the sample query scripts contained in the "sample queries" directory

## Questions/Comments? ##
gary.perkins1164@gmail.com